# Commerce Bambora Europe

This module integrates
[Bambora Europe API](https://payform.bambora.com/docs/web_payments/?page=full-api-reference)
(Bambora Payform, Paybyway) with Drupal Commerce, providing an off-site
payment method. E-commerce site owners may configure e-payments (online
banks) via Bambora using this module.

Currently supported banks include (as per Bambora Europe API):

* Handelsbanken [https://www.handelsbanken.fi](https://www.handelsbanken.fi)
* Osuuspankki [https://www.op.fi](https://www.op.fi)
* Danske Bank [https://danskebank.fi](https://danskebank.fi)
* S-Pankki [https://www.s-pankki.fi](https://www.s-pankki.fi)
* Säästöpankki [https://www.saastopankki.fi](https://www.saastopankki.fi)
* POP Pankki [https://www.poppankki.fi](https://www.poppankki.fi)
* Aktia [https://www.aktia.fi](https://www.aktia.fi)
* Ålandsbanken [https://www.alandsbanken.fi](https://www.alandsbanken.fi)
* Oma Säästöpankki [https://www.omasp.fi](https://www.omasp.fi)

Bambora allows your customers to pay with the selected online banks in
Drupal Commerce. While Bambora Payform is available in many countries
and currencies at the moment this module supports payments only only in
EUR, and no credit card payments.

## Installation

1. Create an account at  [https://www.bambora.com/](https://www.bambora.com/)
1. Download the module, `compser require drupal/commerce_bambora_europe`.
1. Enable Commerce Bambora Europe (`commerce_bambora_europe`).
2. 1. Add a payment method in `admin/commerce/config/payment-gateways`.
5. Add and configure payment gateway using API keys (Private key, API
   key) from
   [https://payform.bambora.com/merchant/sub-merchants/](https://payform.bambora.com/merchant/sub-merchants/)
6. It is hightly recommended to configure API keys via `settings.php`,
   and set the configuration keys to some other values in order to not
   have the API Private key getting committed to code repository. Keys
   are set per payment method, check the payment method configuration
   form for more details.

## Credit card payments

This is not yet possible to do with this module. Patches or funding the
development work are both welcome.

## Credits

Commerce Bambora Europe integration has been written by:

* Perttu Ehn - https://drupal.org/u/rpsu
