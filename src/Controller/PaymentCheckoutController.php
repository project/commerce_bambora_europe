<?php

namespace Drupal\commerce_bambora_europe\Controller;

use Drupal\commerce_payment\Controller\PaymentCheckoutController as CommercePaymentCheckoutController;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\Core\Access\AccessException;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides checkout endpoints for off-site payments.
 */
class PaymentCheckoutController extends CommercePaymentCheckoutController {

  /**
   * Provides the "return or cancel" checkout payment page.
   *
   * Redirects user either to onReturn or onCancle -route to continue checkout
   * page,.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function onVerify(Request $request, RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    $step_id = $route_match->getParameter('step');

    $this->validateStepId($step_id, $order);
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin instanceof OffsitePaymentGatewayInterface) {
      throw new AccessException('The payment gateway for the order does not implement ' . OffsitePaymentGatewayInterface::class);
    }
    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
    $checkout_flow = $order->get('checkout_flow')->entity;
    $checkout_flow_plugin = $checkout_flow->getPlugin();

    try {
      $payment_gateway_plugin->onVerifyPayment($order, $request, $route_match);
      $redirect_step_id = $checkout_flow_plugin->getNextStepId($step_id);
    }
    catch (PaymentGatewayException $e) {
      $this->logger->error($e->getMessage());
      // The Payment Gateway plugin should take care of notifying user.
      $redirect_step_id = $checkout_flow_plugin->getPreviousStepId($step_id);
    }

    $checkout_flow_plugin->redirectToStep($redirect_step_id);
  }

}
