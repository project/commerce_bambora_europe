<?php

namespace Drupal\commerce_bambora_europe\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * Configuration key payment_method_types is left undefined (and let it
 * use the default value 'credit_card') because it applies only on-site payment
 * methods.
 *
 * @CommercePaymentGateway(
 *   id = "bambora_offsite_redirect",
 *   label = "Bambora Europe (Off-site redirect)",
 *   display_label = "Online payment via Bambora",
 *   forms = {
 *     "offsite-payment" =
 *   "Drupal\commerce_bambora_europe\PluginForm\OffsiteRedirect\BamboraOffsitePaymentForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class OffsiteRedirect extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_version' => 'w3',
      'api_key' => '',
      'private_key' => '',
      'all_banks' => TRUE,
      'selected_banks' => [],
      'fallback_language' => 'en',
      'token_validity' => '900',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if ($this->getGatewayInstanceId()) {
      $config_id = $this->getGatewayInstanceId();
    }
    else {
      $config_id = $form_state->getValue('id');
    }
    $api_credentials = $this->getCredentialsFromSettings($config_id);

    if ($config_id) {
      if (!empty($api_credentials['bambora_private_key'])) {
        $this->messenger()
          ->addMessage($this->t('Bambora Europe API private key is set via settings.php.'));
      }
      else {
        $this->messenger()
          ->addError($this->t('Bambora Europe API private key must be set via settings.php.'));
      }
      if (!empty($api_credentials['bambora_api_key'])) {
        $this->messenger()
          ->addMessage($this->t('Bambora Europe API key is set via settings.php.'));
      }
      else {
        $this->messenger()
          ->addWarning($this->t('Bambora Europe API key should be set via settings.php.'));
      }
    }

    $form = parent::buildConfigurationForm($form, $form_state);

    $api_key_description = $this->t('The sub-merchant api key which is associated with the used private key. The sub-merchant api key is listed in the sub-merchants page in the merchant UI in Bambora.');
    if (empty($api_credentials['bambora_api_key'])) {
      $api_key_description .= '< br />' . $this->t('This can and should also be set via <code>settings.php</code>. Use <ul><li><code>:prod</code> for production mode and</li><li><code>:test</code> for test mode</li></ul>', [
        ':prod' => "\$settings['bambora_offsite_redirect']['GATEWAY-MACHINE-NAME.bambora_api_key.live']",
        ':test' => "\$settings['bambora_offsite_redirect']['GATEWAY-MACHINE-NAME.bambora_api_key.test']",
      ]);
    }
    else {
      $api_key_description .= '<br />' . $this->t('<strong>Your API key is set</strong> (in <code>settings.php</code>). Using<ul><li> <code>:prod</code> for production mode and </li><li><code>:test</code> for test mode</li></ul>Changing the value here has no effect.', [
        ':prod' => "\$settings['bambora_offsite_redirect']['" . $config_id . ".bambora_api_key.live']",
        ':test' => "\$settings['bambora_offsite_redirect']['" . $config_id . ".bambora_api_key.test']",
      ]);
    }

    $api_private_key_description = $this->t('The private key is used to encrypt communications with Bambora Europe API.');
    if (empty($api_credentials['bambora_private_key'])) {
      $api_private_key_description .= '< br />' . $this->t('This can and should also be set via <code>settings.php</code>. Use <ul><li><code>:prod</code> for production mode and</li><li><code>:test</code> for test mode</li></ul>', [
        ':prod' => "\$settings['bambora_offsite_redirect']['GATEWAY-MACHINE-NAME.bambora_private_key.live']",
        ':test' => "\$settings['bambora_offsite_redirect']['GATEWAY-MACHINE-NAME.bambora_private_key.test']",
      ]);
    }
    else {
      $api_key_description .= '<br />' . $this->t('<strong>Your API Private key is set</strong> (in <code>settings.php</code>). Using<ul><li><code>:prod</code> for production mode and </li><li><code>:test</code> for test mode</li></ul>Changing the value here has no effect.', [
        ':prod' => "\$settings['bambora_offsite_redirect']['" . $config_id . ".bambora_private_key.live']",
        ':test' => "\$settings['bambora_offsite_redirect']['" . $config_id . ".bambora_private_key.test']",
      ]);
    }
    $form['api_version'] = [
      '#type' => 'select',
      '#title' => $this->t('Bambora Europe API'),
      '#options' => array_combine($this->getBamboraApiVersions(), $this->getBamboraApiVersions()),
      '#default_value' => $this->configuration['api_version'],
      '#required' => TRUE,
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $api_key_description,
      '#default_value' => $this->configuration['api_key'],
    ];
    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API private key'),
      '#description' => $api_private_key_description,
      '#default_value' => $this->configuration['private_key'],
    ];
    $form['token_validity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment token validity (seconds)'),
      '#description' => $this->t('How long payment token should be valid. Default is 900 seconds (15 minutes). Token is requested right before the redirect to the payment page.'),
      '#default_value' => $this->configuration['token_validity'],
      '#required' => TRUE,
      '#pattern' => '[0-9]+',
    ];
    $form['fallback_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Fallback language'),
      '#description' => $this->t('Language to use if user account language is not among allowed values. Bambora supports only these languages.'),
      '#default_value' => $this->configuration['fallback_language'],
      '#options' => $this->getBamboraApiLanguages(),
      '#required' => TRUE,
    ];
    $form['banks'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Enabled banks'),
      '#description' => $this->t('Select which banks are enabled.'),
    ];
    $form['banks']['all_banks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('All banks'),
      '#description' => $this->t('Enable all available banks in Bambora. <strong>Note: </strong> Bambora Europe API can handle only EUR payments for e-payments (banks).'),
      '#default_value' => $this->configuration['all_banks'],
    ];
    $form['banks']['selected_banks'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Selected banks'),
      '#description' => $this->t('Select banks to enable in Bambora.'),
      '#default_value' => $this->configuration['selected_banks'],
      '#options' => $this->getAvailableBanks(),
      '#states' => [
        'visible' => [
          ':input[name="configuration[bambora_offsite_redirect][banks][all_banks]"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $values['token_validity'] = trim($values['token_validity']);

    if ((int) $values['token_validity'] != $values['token_validity']
      || $values['token_validity'] < 0) {
      $form_state->setErrorByName('token_validity', $this->t('Token validity must be a positive number.'));
    }
    if ((int) $values['token_validity'] != (int) $values['token_validity']) {
      $form_state->setErrorByName('token_validity', $this->t('Token validity must be a positive number.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $values['banks']['selected_banks'] = array_filter($values['banks']['selected_banks']);
      $this->configuration['api_version'] = $values['api_version'];
      $this->configuration['api_key'] = trim($values['api_key']);
      $this->configuration['private_key'] = trim($values['private_key']);
      $this->configuration['all_banks'] = $values['banks']['all_banks'];
      $this->configuration['selected_banks'] = array_values(array_filter($values['banks']['selected_banks']));
      if ($this->configuration['all_banks']) {
        $this->configuration['selected_banks'] = [];
      }
      $this->configuration['fallback_language'] = $values['fallback_language'];
      $this->configuration['token_validity'] = trim($values['token_validity']);
    }
  }

  /**
   * Negotiate a valid language code for Bambora Europe API use.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return string
   *   Languge code for API use,.
   */
  public function negotiateValidLanguageCode(OrderInterface $order) {
    $language = $order->getCustomer()->getPreferredLangcode(TRUE);
    $api_languages = $this->getBamboraApiLanguages();
    if (!array_key_exists($language, $api_languages)) {
      return $this->configuration['fallback_language'];
    }
    return $language;
  }

  /**
   * Negotiate payment type.
   *
   * Possible values are e-payment, embedded and terminal.
   *  - 'e-payment' is used for banking payments and creditcard form in pay page
   *  - 'embedded' is used for the inline card form.
   *
   * @return string|null
   *   Payment
   */
  public function getBamboraPaymentType() {
    if (count($this->configuration['selected_banks']) ||
      $this->configuration['all_banks']) {
      return 'e-payment';
    }
    return NULL;
  }

  /**
   * Provides a list of banks to select.
   *
   * @return array
   *   Array of banks to select, keyed by the Bambora -used machine name.
   */
  private function getAvailableBanks() {
    return [
      'handelsbanken' => $this->t('Handelsbanken <a href=":url">https://www.handelsbanken.fi</a>', [
        ':url' => 'https://www.handelsbanken.fi',
      ]),
      'osuuspankki' => $this->t('Osuuspankki <a href=":url">https://www.op.fi</a>', [
        ':url' => 'https://www.op.fi',
      ]),
      'danskebank' => $this->t('Danske Bank <a href=":url">https://danskebank.fi</a>', [
        ':url' => 'https://danskebank.fi',
      ]),
      'spankki' => $this->t('S-Pankki <a href=":url">https://www.s-pankki.fi</a>', [
        ':url' => 'https://www.s-pankki.fi',
      ]),
      'saastopankki' => $this->t('Säästöpankki <a href=":url">https://www.saastopankki.fi</a>', [
        ':url' => 'https://www.saastopankki.fi',
      ]),
      'paikallisosuuspankki' => $this->t('POP Pankki <a href=":url">https://www.poppankki.fi</a>', [
        ':url' => 'https://www.poppankki.fi',
      ]),
      'aktia' => $this->t('Aktia <a href=":url">https://www.aktia.fi</a>', [
        ':url' => 'https://www.aktia.fi',
      ]),
      'alandsbanken' => $this->t('Ålandsbanken <a href=":url">https://www.alandsbanken.fi</a>', [
        ':url' => 'https://www.alandsbanken.fi',
      ]),
      'omasaastopankki' => $this->t('Oma Säästöpankki <a href=":url">https://www.omasp.fi</a>', [
        ':url' => 'https://www.omasp.fi',
      ]),
    ];
  }

  /**
   * Provides a list of available Bambora Europe API versions.
   *
   * @return array
   *   Array of API versions, keyed by the Bambora Europe API version names.
   */
  private function getBamboraApiVersions() {
    return ['w3.1'];
  }

  /**
   * Provides a list of languages supported by Bambora AP.
   *
   * @return array
   *   Array of languages, keyed by the Bambora Europe API version names.
   */
  public function getBamboraApiLanguages() {
    return [
      'en' => $this->t('English'),
      'fi' => $this->t('Finnish'),
      'ru' => $this->t('Russian'),
      'sv' => $this->t('Swedish'),
    ];
  }

  /**
   * Processes the Bambora Europe API return request.
   *
   * This method checks the response payload and throws
   * PaymentGatewayException to return user back to review -step or
   * processed payment data if payload is valid and payment was made.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @see https://payform.bambora.com/docs/web_payments/?page=full-api-reference#charge-return-from-redirect
   */
  public function onVerifyPayment(OrderInterface $order, Request $request, RouteMatchInterface $route_match) {

    try {
      $this->validateReturnRequest($order, $request);
    }
    catch (PaymentGatewayException $e) {
      throw $e;
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');

    $logger = \Drupal::logger('commerce_bambora_europe');

    switch ($request->query->get('RETURN_CODE')) {
      case "0":
        // Payment completed successfully.
        $logger->info('Saving Payment information. Order number: ' .
          $order->id() . ' ... starting.');

        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
        $payment = $payment_storage->create(
          [
            'state' => 'completed',
            // Total price number was sent with the request (and is part of
            // the data used to get the token), so we can trust the
            // order total price to be still the same.
            'amount' => $order->getTotalPrice(),
            'payment_gateway' => $this->entityId,
            'order_id' => $order->id(),
            'remote_id' => 'Order number ' . $order->id(),
            'remote_state' => 'payment completed successfully',
          ]);
        $payment->save();
        $logger->info('Saving Payment information. Order number: ' .
          $order->id() . ' ... done.');
        $this->messenger()
          ->addStatus($this->t('Payment was processed.'));
        break;

      case "1":
        // Payment failed. Customer did not successfully finish the payment.
        // Bambora has no 'cancel_url' property, but user is sent always
        // back to the return_url -route.
        // @see \Drupal\commerce_bambora_europe\PluginForm\OffsiteRedirect\BamboraOffsitePaymentForm::getPaymentObject().
        $logger->info('Payment was cancelled. Order number: ' .
          $order->id() . '.');
        $this->messenger()
          ->addWarning($this->t('Payment was cancelled.'));
        throw new PaymentGatewayException(t('Payment was cancelled.'));

      case "4":
        // Transaction status could not be updated after customer returned
        // from the web page of a bank. Please use the merchant UI to resolve
        // the payment status.
        $logger->info('Transaction status could not be updated after customer returned from the web page of a bank. Please use the merchant UI to resolve the payment status. Order number: ' .
          $order->id() . '.');
        $this->messenger()
          ->addError($this->t('Payment status is unclear. Please contact our customer support.'));
        throw new PaymentGatewayException('Transaction status could not be updated after customer returned from the web page of a bank.');

      case "10":
        // Transaction status could not be updated after customer returned
        // from the web page of a bank. Please use the merchant UI to resolve
        // the payment status.
        $logger->info('Payment gateway is temporarily closed for maintenance. Order number: ' .
          $order->id() . '.');
        $this->messenger()
          ->addError($this->t('Payment gateway is temporarily closed for maintenance. Please try again later.'));
        throw new PaymentGatewayException('Payment gateway is temporarily closed for maintenance. Order number: ' .
          $order->id() . '.');
    }
  }

  /**
   * {@inheritdoc}
   *
   * Redirect users landing here to the correct route. This route is blocked.
   */
  public function onReturn(OrderInterface $order, Request $request) {

    $logger = \Drupal::logger('commerce_bambora_europe');
    $logger->warning('Return URL from payment gateway is using a wrong route: '
      . $request->attributes->get('_route'));

    $options = [
      'absolute' => TRUE,
      'query' => $request->query->all(),
    ];
    $route_parameters = [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ];
    $redirect = Url::fromRoute('commerce_bambora_europe.checkout.verify-payment',
      $route_parameters, $options);

    throw new NeedsRedirectException($redirect->toString());
  }

  /**
   * {@inheritdoc}
   *
   * Redirect users landing here to the correct route. This route is blocked.
   */
  public function onCancel(OrderInterface $order, Request $request) {

    $logger = \Drupal::logger('commerce_bambora_europe');
    $logger->warning('Return URL from payment gateway is using a wrong route: '
      . $request->attributes->get('_route'));

    $options = [
      'absolute' => TRUE,
      'query' => $request->query->all(),
    ];
    $route_parameters = [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ];
    $redirect = Url::fromRoute('commerce_bambora_europe.checkout.verify-payment',
      $route_parameters, $options);

    throw new NeedsRedirectException($redirect->toString());
  }

  /**
   * Validate the payload of the returned data..
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   Thrown when the request is invalid or the payment failed.
   */
  public function validateReturnRequest(OrderInterface $order, Request $request) {

    $logger = \Drupal::logger('commerce_bambora_europe');

    // We need to compare received order number to something we have saved
    // upon requesting the payment token.
    // @see Drupal\commerce_bambora_europe\PluginForm\OffsiteRedirect\BamboraOffsitePaymentForm::getOrderIdForAPI()
    // @see Drupal\commerce_bambora_europe\PluginForm\OffsiteRedirect\BamboraOffsitePaymentForm::buildConfigurationForm()
    $data = $order->getData('bambora_payment_api_object', []);
    $order_number = isset($data['order_number']) ? $data['order_number'] : '';

    if ($request->getRealMethod() != 'GET') {
      $method = $request->getRealMethod();
      $logger->warning('Return call from payment gateway is using a wrong HTTP method: ' . $request->getRealMethod());
      throw new PaymentGatewayException(self::class . '::' . __FUNCTION__ . '(): received a request with a mismacthing HTTP method (recieved ' . $method . ')');
    }

    if ($order_number != $request->query->get('ORDER_NUMBER')) {
      $logger->warning('Return call from payment gateway has a wrong ORDER_NUMBER value: ' . $request->query->get('ORDER_NUMBER') . ' instead of ' . $order->id());
      throw new PaymentGatewayException(self::class . '::' . __FUNCTION__ . '(): order number mismatch.');
    }

    if (!$this->validateResponseAuthCode($request)) {
      $logger->warning('The AUTHCODE in the return call from payment gateway does not validate.');
      throw new PaymentGatewayException(self::class . '::' . __FUNCTION__ . '(): authcode failed validation.');
    }
  }

  /**
   * {@inheritdoc}
   */
  /*
  public function onNotify(Request $request) {
  // @TODO This is needed, but only after implementing card payment options.
  return parent::onNotify($request);
  }
   */

  /**
   * Get Payment gateway instance id, if set.
   *
   * @return string|null
   *   Config entity ID.
   */
  public function getGatewayInstanceId() {
    if ($this->parentEntity) {
      return $this->parentEntity->id();
    }

    return NULL;
  }

  /**
   * Get API credentials from Settings, if present.
   *
   * Must get the config entity id since it appears not to be preset within
   * the plugin instance, and not available in some cases.
   *
   * Find out if the credentials are set via settings.php.
   * This allows both current and deprecated structures:
   * <code>
   *   $settings[bambora_api_key] = ... #DEPRECATED
   *   $settings['bambora_offsite_redirect']['GATEWAY-MACHINE-NAME.bambora_api_key.live'] = ...
   *   $settings['bambora_offsite_redirect']['GATEWAY-MACHINE-NAME.bambora_api_key.test'] = ...
   * </code>
   *
   * @param string $config_entity_id
   *   Config entity ID.
   *
   * @return array|null
   *   Array of credentials with:
   *   - bambora_api_key
   *   - bambora_private_key
   *   Or NULL if nothing found or config id could not be figured out.
   */
  private function getCredentialsFromSettings($config_entity_id = '') {
    $credentials = [];
    if (!$config_entity_id && !$config_entity_id = $this->getGatewayInstanceId()) {
      return NULL;
    }
    if (!$mode = $this->configuration['mode']) {
      return NULL;
    }
    $api_credentials = Settings::get($this->getPluginId());

    $keys = ['bambora_api_key', 'bambora_private_key'];
    foreach ($keys as $key) {
      $needle = $config_entity_id . '.' . $key . '.' . $mode;
      if (is_array($api_credentials) &&
        !empty($api_credentials[$needle])) {
        $credentials[$key] = $api_credentials[$needle];
      }
      elseif (Settings::get($key)) {
        $credentials[$key] = Settings::get($key);
      }
    }

    return !empty($credentials) ? $credentials : NULL;
  }

  /**
   * Get active API key.
   *
   * This can be set via settings.php OR via config.
   */
  public function getFinalApiKey() {
    $api_credentials = $this->getCredentialsFromSettings();
    $key = 'bambora_api_key';

    if (is_array($api_credentials) && isset($api_credentials[$key])) {
      return $api_credentials[$key];
    }

    return $this->configuration['api_key'];
  }

  /**
   * Get active API private key.
   *
   * This can be set via settings.php OR via config.
   */
  public function getFinalPrivateKey() {
    $api_credentials = $this->getCredentialsFromSettings();
    $key = 'bambora_private_key';

    if (is_array($api_credentials) && isset($api_credentials[$key])) {
      return $api_credentials[$key];
    }

    return $this->configuration['private_key'];
  }

  /**
   * Validate the return request AuthCode.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return bool
   *   Whether authcode is valid or not.
   *
   * @see https://payform.bambora.com/docs/web_payments/?page=full-api-reference#charge-return-from-redirect
   */
  public function validateResponseAuthCode(Request $request) {

    $authcode = $request->query->get('AUTHCODE');

    if (empty($authcode)) {
      return FALSE;
    }

    $fields = [
      'RETURN_CODE',
      'ORDER_NUMBER',
      'SETTLED',
      'CONTACT_ID',
      'INCIDENT_ID',
    ];
    $unencoded = [];
    // Add each field to to-be-hashed -string only if they have some content.
    foreach ($fields as $field) {
      // RETURN_CODE may have value of '0'.
      if ($request->query->get($field, FALSE) !== FALSE) {
        $unencoded[$field] = $request->query->get($field);
      }
    }
    $string = implode('|', $unencoded);
    $hash = hash_hmac('sha256', $string, $this->getFinalPrivateKey());
    $hash = strtoupper($hash);

    return $authcode === $hash;
  }

}
